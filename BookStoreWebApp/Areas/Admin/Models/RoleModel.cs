﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStoreWebApp.Areas.Admin.Models
{
    public class RoleModel
    {
        public string RoleName { get; set; }
    }
}