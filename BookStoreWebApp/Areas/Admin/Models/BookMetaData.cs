﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreWebApp.Areas.Admin.Models
{
    class BookMetaData
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        [Range(0,1000)]
        [Display (Name ="Price")]
        public Nullable<decimal> Price { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
        [DisplayFormat(DataFormatString ="{0:dd-MM-yyyy}")]
        [Display(Name ="Date Of Publish")]
        public Nullable<System.DateTime> DateOfPublish { get; set; }
        [Required]
        public Nullable<int> AuthorID { get; set; }
        [Required]
        public Nullable<int> CategoryID { get; set; }
        [Required]
        public Nullable<int> Stock { get; set; }
        [Required]
        [Range(0,100)]
        public Nullable<int> Rating { get; set; }

    }
}
