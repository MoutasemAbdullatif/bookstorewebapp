﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStoreWebApp.Areas.Admin.Models
{
    public class BookDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Description { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public HttpPostedFileBase PhotoHover { get; set; }
        public Nullable<System.DateTime> DateOfPublish { get; set; }
        public Nullable<int> Stock { get; set; }
        public Nullable<int> Rating { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorLastName { get; set; }
        public string Category { get; set; }
    }
}