﻿using BookStoreWebApp.Areas.Admin.Models;
using BookStoreWebApp.Controllers;
using BookStoreWebApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStoreWebApp.Areas.Admin.Controllers
{
    public class RoleController : AdminController
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/Role
        public ActionResult Index()
        {
           
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var model = roleManager.Roles.ToList();
            return View(model);
        }
        public ActionResult AddRole()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddRole(RoleModel role)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            
           if (roleManager.RoleExists(role.RoleName) == false)
          {
           roleManager.Create(new IdentityRole(role.RoleName));
          }
            
             return RedirectToAction("Index");
        }

        public ActionResult RoleUser()
        {
            return View();
        }


        [HttpPost]
        public ActionResult RoleUser(AddRoleUser roleUser)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);


            var kullanici = userManager.FindByName(roleUser.UserName);

            if (!userManager.IsInRole(kullanici.Id, roleUser.RoleName))
            {
                userManager.AddToRole(kullanici.Id, roleUser.RoleName);
            }

            return RedirectToAction("Index");
        }
}
}