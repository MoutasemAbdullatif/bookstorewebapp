﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookStoreWebApp;
using BookStoreWebApp.Areas.Admin.Models;
using System.IO;
using System.Data.Entity.Validation;
using System.Diagnostics;
using BookStoreWebApp.Controllers;

namespace BookStoreWebApp.Areas.Admin.Controllers
{
    public class BookAdminController : AdminController
    {
        private BookStoreMDFEntities db = new BookStoreMDFEntities();

        // GET: Admin/BookAdmin
        public ActionResult Index()
        {
            var books = db.Books.Include(b => b.Author).Include(b => b.Category);
            return View(books.ToList());
        }

        // GET: Admin/BookAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/BookAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BookDTO model)
        {
            const string folderpath = "/Content/images/";
            if (ModelState.IsValid)
            {

                if (model.Photo != null && model.PhotoHover != null && model.Photo.ContentLength > 0 && model.PhotoHover.ContentLength > 0)
                {
                    var photopath = Path.Combine(Server.MapPath(folderpath), model.Photo.FileName);
                    var photopathhover = Path.Combine(Server.MapPath(folderpath), model.PhotoHover.FileName);
                    model.Photo.SaveAs(photopath);
                    model.PhotoHover.SaveAs(photopathhover);
                }
                Author author=new Author();
                Category category = new Category();
                Book book = new Book();
                book.Title = model.Title;
                book.Price = model.Price;
                book.Description = model.Description;
                book.DateOfPublish = model.DateOfPublish;
                book.Stock = model.Stock ?? 1;
                book.Rating = model.Rating ?? 1;
                if(!db.Authors.Any(x=>x.FirstName==model.AuthorFirstName&&x.LastName==model.AuthorLastName))
                {
                    author.FirstName = model.AuthorFirstName;
                    author.LastName = model.AuthorLastName; 
                    db.Authors.Add(author);
                    db.SaveChanges();
                }
                else
                {
                    author = db.Authors.First(x=>x.FirstName==model.AuthorFirstName);
                }
                if (!db.Categories.Any(x=>x.CategoryName==model.Category))
                {
                    category.CategoryName = model.Category;
                    db.Categories.Add(category);
                    db.SaveChanges();
                }
                else
                {
                    category = db.Categories.First(x => x.CategoryName == model.Category);
                }
                book.CategoryID = category.ID;
                book.AuthorID = author.ID;
                book.PhotoUrl = folderpath + model.Photo.FileName;
                book.PhotoUrlHover = folderpath + model.PhotoHover.FileName;
                try {
                    db.Books.Add(book);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                                    validationError.PropertyName,
                                                    validationError.ErrorMessage);
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: Admin/BookAdmin/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            BookDTO dto = new BookDTO();
            dto.ID = book.ID;
            dto.Title = book.Title;
            dto.Price = book.Price;
            dto.Description = book.Description;
            dto.Category = book.Category.CategoryName;
            dto.AuthorFirstName = book.Author.FirstName;
            dto.AuthorLastName = book.Author.LastName;
            dto.DateOfPublish = book.DateOfPublish;
            dto.Stock = book.Stock;
            dto.Rating = book.Rating;
            return View(dto);
        }

        // POST: Admin/BookAdmin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BookDTO model)
        {
            const string folderpath = "~/Content/images/";
            if (ModelState.IsValid)
            {
                Author author = new Author();
                Category category = new Category();
                Book book = new Book();
                if (model.Photo != null && model.PhotoHover != null && model.Photo.ContentLength > 0 && model.PhotoHover.ContentLength > 0)
                {
                    var photopath = Path.Combine(Server.MapPath(folderpath), model.Photo.FileName);
                    var photopathhover = Path.Combine(Server.MapPath(folderpath), model.PhotoHover.FileName);
                    model.Photo.SaveAs(photopath);
                    model.PhotoHover.SaveAs(photopathhover);
                }
                book.ID = model.ID;
                book.Title = model.Title;
                book.Price = model.Price;
                book.Description = model.Description;
                book.DateOfPublish = model.DateOfPublish;
                book.Stock = model.Stock;
                book.Rating = model.Rating;
                if (!db.Authors.Any(x => x.FirstName == model.AuthorFirstName && x.LastName == model.AuthorLastName))
                {
                    author.FirstName = model.AuthorFirstName;
                    author.LastName = model.AuthorLastName;
                    db.Authors.Add(author);
                    db.SaveChanges();
                }
                else
                {
                    author = db.Authors.First(x => x.FirstName == model.AuthorFirstName);
                }
                if (!db.Categories.Any(x => x.CategoryName == model.Category))
                {
                    category.CategoryName = model.Category;
                    db.Categories.Add(category);
                    db.SaveChanges();
                }
                else
                {
                    category = db.Categories.First(x => x.CategoryName == model.Category);
                }
                book.CategoryID = category.ID;
                book.AuthorID = author.ID;
                if (model.Photo != null && model.Photo.ContentLength > 0) { 
                book.PhotoUrl = folderpath + model.Photo.FileName;
                }
                if (model.PhotoHover != null && model.PhotoHover.ContentLength > 0) { 
                book.PhotoUrlHover = folderpath + model.PhotoHover.FileName;
                }
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Admin/BookAdmin/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Admin/BookAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
