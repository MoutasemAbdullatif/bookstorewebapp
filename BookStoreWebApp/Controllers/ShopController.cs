﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using PagedList;
using System.Net;

namespace BookStoreWebApp.Controllers
{
   
    public class ShopController : Controller
    {
        private BookStoreMDFEntities db = new BookStoreMDFEntities();
        // GET: Shop
        public ActionResult Index(string sortOrder,string currentFilter,  string searchString,int? page)
        {

            ViewBag.CurrentSort = sortOrder;

            if (searchString!=null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            var books = db.Books.Include("Category").ToList();

            if(!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(x => x.Title.ToUpper().Contains(searchString.ToUpper()) || x.Category.CategoryName.ToUpper().Contains(searchString.ToUpper())).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    books = books.OrderByDescending(x => x.Title).ToList();
                    break;
                case "Date":
                    books = books.OrderBy(x => x.DateOfPublish).ToList();
                    break;
                case "date_desc":
                    books = books.OrderByDescending(x => x.DateOfPublish).ToList();
                    break;
                default:
                    books = books.OrderBy(x => x.Title).ToList();
                    break;
            }
           
            int pageNumber = (page ?? 1);
            return View(books.ToPagedList(pageNumber, 6));
        }
        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book= db.Books.Find(id);
            if (book== null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        [ChildActionOnly]
        public ActionResult Related(string category)
        {
            var books = db.Books.Select(x => x);
            if (!String.IsNullOrEmpty(category))
            {
                books = books.Where(x => x.Category.CategoryName == category).Take(3);
            }
            else
            {
                books = books.Take(3);
            }
            return View(books);
        }
        [Authorize(Roles ="user")]
        public ActionResult CartAction()
        {
            return View();
        }
    }
}