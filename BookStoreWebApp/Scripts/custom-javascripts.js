﻿$(function () {

    // Slider Revolution
    jQuery('#slider-rev').revolution({
        delay: 5000,
        startwidth: 870,
        startheight: 520,
        onHoverStop: "true",
        hideThumbs: 250,
        navigationHAlign: "center",
        navigationVAlign: "bottom",
        navigationHOffset: 0,
        navigationVOffset: 15,
        soloArrowLeftHalign: "left",
        soloArrowLeftValign: "center",
        soloArrowLeftHOffset: 0,
        soloArrowLeftVOffset: 0,
        soloArrowRightHalign: "right",
        soloArrowRightValign: "center",
        soloArrowRightHOffset: 0,
        soloArrowRightVOffset: 0,
        touchenabled: "on",
        stopAtSlide: -1,
        stopAfterLoops: -1,
        dottedOverlay: "none",
        fullWidth: "on",
        spinned: "spinner4",
        shadow: 3, // 1 2 3 to change shadows
        hideTimerBar: "on",
        // navigationStyle:"preview2"
    });

    /* This is fix for mobile devices position slider at the top  via absolute pos */
    var fixSliderForMobile = function () {
        var winWidth = $(window).width();

        if (winWidth <= 767 && $('#slider-rev-container').length) {
            var revSliderHeight = $('#slider-rev').height();
            console.log(revSliderHeight);
            $('.slider-position').css('padding-top', revSliderHeight);
            $('.main-content').css('position', 'static');
        } else {
            $('.slider-position').css('padding-top', 0);
            $('.main-content').css('position', 'relative');
        }
    };

    fixSliderForMobile();

    /* Resize fix positionin */
    if ($.event.special.debouncedresize) {
        $(window).on('debouncedresize', function () {
            fixSliderForMobile();
        });
    } else {
        $(window).on('resize', function () {
            fixSliderForMobile();
        });
    }


});